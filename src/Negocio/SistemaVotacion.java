package Negocio;

import Entidad.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class SistemaVotacion {

    private ListaCD<Departamento> dptos = new ListaCD();
    private ListaCD<Persona> personas = new ListaCD();
    private Cola<Notificacion> notificaciones = new Cola();

    public SistemaVotacion(String urlDptos, String urlMunicipios, String urlPer, String urlCentros) {
        crearDptos(urlDptos);
        crearMunicipios(urlMunicipios);
        crearPersonas(urlPer);
        crearCentrosVotacion(urlCentros);
    }
    private Persona getPersonaAleatorio(){
      int p= (int)(Math.random()*((this.personas.getTamanio()-1)-0+1)+0);
     return this.personas.get(p) ;
    }
    
    public void asignarJurados() {
        Persona dato= getPersonaAleatorio();
            Municipio id = this.getMunicipio(dato.getId_Municipio_inscripcion());
            CentroVotacion centro = id.getAleatorio();
            if (centro != null) {
                Mesa mesa = centro.getAleatoria();
                if (!mesa.tieneJurados()) {
                    mesa.asignarJurado(dato);
                 Notificacion n = new Notificacion("",id.getNombre() ,crearFecha("2019-10-27"), dato) ;
                 n.crearNotificacion(centro, mesa);
                 this.notificaciones.enColar(n);
                }
                //crear la cola de notificaciones
            }
    }

    public void asignarSufragantes() {
       Persona dato= getPersonaAleatorio();
            Municipio id = this.getMunicipio(dato.getId_Municipio_inscripcion());
            CentroVotacion centro = id.getAleatorio();
            if (centro != null) {
                Mesa mesa = centro.getAleatoria();
                if (mesa.tieneJurados()) {
                   mesa.asignarSufragantes(dato);
                   
                }
            }
    }

    private void crearCentrosVotacion(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int idCentro = Integer.parseInt(datos2[0]);
            String nombre = datos2[1];
            String direccion = datos2[2];
            int idMun = Integer.parseInt(datos2[3]);
            int canMesas = Integer.parseInt(datos2[4]);
            int maxSufra = Integer.parseInt(datos2[5]);
            Municipio muni = this.getMunicipio(idMun);
            if (muni != null) {
                CentroVotacion centro = new CentroVotacion(idCentro, nombre, direccion, maxSufra);
                centro.crearMesas(canMesas);
                muni.getCentros().insertarAlFinal(centro);
            }

        }

    }

    private Municipio getMunicipio(int id) {
        for (Departamento dato : this.dptos) {
            Municipio muni = dato.getMunicipio(id);
            if (muni != null) {
                return muni;
            }
        }
        return null;
    }

    private void crearPersonas(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            long cedula = Long.parseLong(datos2[0]);
            String nombre = datos2[1];
            LocalDateTime fechaNacimiento = crearFecha(datos2[2]);
            int idMuni = Integer.parseInt(datos2[3]);
            String email = datos2[4];
            Persona nueva = new Persona(cedula, nombre, fechaNacimiento, idMuni, email);
            this.personas.insertarAlFinal(nueva);
        }
    }

    private LocalDateTime crearFecha(String fecha) {
        String datos3[] = fecha.split("-");
        int agno = Integer.parseInt(datos3[0]);
        int mes = Integer.parseInt(datos3[1]);
        int dia = Integer.parseInt(datos3[2]);

        return LocalDateTime.of(agno, mes, dia, 0, 0, 0, 0);

    }

    private void crearMunicipios(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio    
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            int id_muni = Integer.parseInt(datos2[1]);
            String nombreMun = datos2[2];
            Municipio nuevo = new Municipio(id_muni, nombreMun);

            Departamento dpto = buscarDpto(id_dpto);
            if (dpto != null) {
                dpto.getMunicipios().insertarAlFinal(nuevo);
            }
        }
    }

    private Departamento buscarDpto(int id) {
        for (Departamento dato : this.getDptos()) {
            if (dato.getId_dpto() == id) {
                return dato;
            }
        }
        return null;
    }

    private void crearDptos(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            int id_dpto = Integer.parseInt(datos2[0]);
            Departamento nuevo = new Departamento(id_dpto, datos2[1]);
            this.dptos.insertarAlFinal(nuevo);

        }

    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public String getListadoDpto() {
        String msg = "";
        for (Departamento datos : this.dptos) {
            msg += datos.toString() + "\n";
        }
        return msg;
    }

    public String getListadoPersonas() {
        String msg = "";
        for (Persona datos : this.personas) {
            msg += datos.toString() + "\n";
        }
        return msg;

    }

}
