package Entidad;

import ufps.util.colecciones_seed.Cola;

public class Mesa {

    private int id_mesa;
    private Cola<Persona> sufragantes=new Cola();
    private Persona[] jurados=new Persona[3]; //null, null, null

    public Mesa() {
    }

    public Mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }

    public int getId_mesa() {
        return id_mesa;
    }

    public void setId_mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }

    public Cola<Persona> getSufragantes() {
        return sufragantes;
    }

    public void setSufragantes(Cola<Persona> sufragantes) {
        this.sufragantes = sufragantes;
    }

    public Persona[] getJurados() {
        return jurados;
    }

    public void setJurados(Persona[] jurados) {
        this.jurados = jurados;
    }

    @Override
    public String toString() {
        return "Mesa{" + "id_mesa=" + id_mesa + '}';
    }
   
    
  
    
 public boolean asignarJurado(Persona p)   
 {
 
  if(p.esTerceraEdad())   
      return false;
  
  
  int i=buscarEspacio();
  if(i<0)   
      return false;
  
  this.jurados[i]=p;
  
  p.setEsJurado(true);
  
  return true;
     
 }
  public boolean asignarSufragantes(Persona p){
    if(!p.isEsJurado()){
     if(this.jurados.length == 3){
     p.setEsSufragante(true);
     return true;
     }
    }
    return false;
  }
  
  private int buscarEspacio()  
  {
  for(int i=0;i<this.jurados.length;i++)
    {
        if(this.jurados[i]==null)
            return i;
    }
  return -1;
  }
    
    // p'   p''   p'''
    //    0       1       2
  
  
  public boolean tieneJurados()
  {
      return this.buscarEspacio()==-1;
  }
      public Mesa getAleatoria() {

        if (this.getMesas().esVacia()) {
            Pila<Mesa> aux = new Pila();
            int p = (int) ((Math.random()) * (this.mesas.getTamanio() - 1 + 1) + 1);
            while(!this.mesas.esVacia()){
            if(p==1){
               cargarPila(p);
               return this.mesas.desapilar();
            }   
            p.apilar(this.mesas.desapilar());
            i--;
             
            

        }

       
    }
         return null;
 }
    private Pila<Mesa> cargarPila(Pila<Mesa>p){
    while(!p.esVacia()){
        this.mesas.apilar(p.desapilar);
    }
}
}
