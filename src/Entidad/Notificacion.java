package Entidad;


import java.time.LocalDateTime;

public class Notificacion {

    private String nombreDpto;

    private String nombreMunicipio;

    private LocalDateTime fechaVotacion;
    
    private Persona persona;

    public Notificacion() {
    }

    public Notificacion(String nombreDpto, String nombreMunicipio, LocalDateTime fechaVotacion, Persona persona) {
        this.nombreDpto = nombreDpto;
        this.nombreMunicipio = nombreMunicipio;
        this.fechaVotacion = fechaVotacion;
        this.persona = persona;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public LocalDateTime getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(LocalDateTime fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void crearNotificacion(CentroVotacion centro, Mesa mesa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
